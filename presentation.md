# IPFS

## InterPlanetary File System
## The Distributed Web

Ein Überblick

2016-09-09

[VALUG.at](https://valug.at/)

Wolfgang Silbermayr

Quelltext: <https://gitlab.com/valug/ipfs-slides>

Lizenz der Präsentation: [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

![CC-BY-SA 4.0](images/cc-by-sa.png)

---

# Inhalt

- Anlaufstellen
- Installation und Starten
- Was macht IPFS
- Verwendung
- Name-Service IPNS
- Technische Hintergründe
- Darauf Aufbauende Projekte / Ideen
- Webhosting mit IPFS

---

# Anlaufstellen

- Webseite: <https://ipfs.io/>
- Download: <https://dist.ipfs.io/>
- Public Gateway: <https://gateway.ipfs.io/>
- Github: <https://github.com/ipfs/>
- IRC: `irc://irc.freenode.net/#ipfs`

---

# Installation und Starten I

- Install Guide: <https://ipfs.io/docs/install/>
- Download von: <https://dist.ipfs.io/#go-ipfs>
- In ein Verzeichnis (nachfolgend `$IPFSDIR` genannt) entpacken
- Checken, ob es grundsätzlich läuft: `$IPFSDIR/ipfs --help` ausführen
- Hilfreich:
    - Die Binärdatei in `$PATH` kopieren oder verlinken
    - Alternativ: (temporär) die `$PATH`-Umgebungsvariable um das entpackte Verzeichnis erweitern
    - Danach kann einfach `ipfs` anstatt von `$IPFSDIR/ipfs` verwendet werden

---

# Installation und Starten II

- IPFS-Repository initialisieren: `ipfs init`
    - Erstellt ein Repository in `~/.ipfs`
- IPFS-Daemon starten: `ipfs daemon`
    - Für mehr Gesprächigkeit: `IPFS_LOGGING=info ipfs daemon`
    - Der Daemon läuft standardmäßig im Vordergrund
- Weboberfläche ist unter <http://localhost:5001/webui/> erreichbar
- Gateway ist unter <http://localhost:8080/ipfs/<identifier\>> erreichbar
- Public Gateway ohne Installation <http://gateway.ipfs.io/ipfs/<identifier\>> erreichbar

---

# Was macht IPFS

- File-Sharing ähnlich zu BitTorrent
- Identifiziert Dateien durch interplanetar ziemlich eindeutige Identifier
    - Format: [QmYWAifyw2V5dEq7c5GgdSPffeKoYXQZggnYzw5RbXpig4](http://localhost:8080/ipfs/QmYWAifyw2V5dEq7c5GgdSPffeKoYXQZggnYzw5RbXpig4)
- Deduplikation auf Ebene von Blöcken, dadurch auch für Dateien
- Verzeichnishierarchien können rekursiv hinzugefügt werden
- Heruntergeladene Daten werden lokal im Cache behalten
- Cache wird vom Daemon je nach Bedarf geleert
- Keine Garantie für Verfügbarkeit ("austrocknen")
- Explizites "Pinning" erlaubt permanentes Behalten von Daten
- Vom restlichen Netz getrennte Inseln können die zwischengespeicherten Daten weiterhin austauschen

---

# Verwendung (I)

## Dateien oder Verzeichnisse herunterladen

`ipfs get <identifier>`

## Dateien ins Repository laden

`ipfs add <filename>`

## Verzeichnisse rekursiv ins Repository laden

`ipfs add -r <dirname>`

## Dateien direkt anzeigen lassen

`ipfs cat <identifier>`

---

# Verwendung (II)

## Dateien "anpinnen"

`ipfs pin add <identifier>`

## Dateien "entpinnen"

`ipfs pin rm <identifier>`

## "Angepinnte" Dateien auflisten

`ipfs pin ls [<identifier>]`

---

# Verwendung (III)

## Und viele weitere Befehle

- Datenstrukturen-Kommandos:
    - `ipfs block`: Interaktionen mit rohen Blöcken
    - `ipfs object`: Interaktionen mit rohen dag-Nodes
    - `ipfs files`: Interaktionen mit Objekten ähnlich Unix-Dateisystemen

- Netzwerk-Kommandos:
    - `ipfs id`: Informationen über den eigenen oder andere Knoten
    - `ipfs diag`: Diagnose
    - `ipfs swarm`: Informationen über den Netzwerkschwarm

---

# Verwendung (IV)

- Fuse-Mount in ein Verzeichnis
    - `ipfs mount -f <ipfs-path> -n <ipns-path>`
- ACHTUNG
    - Relative Pfade werden nicht in Bezug zum aktuellen Verzeichnis des Aufrufers, sondern zum Arbeitsverzeichnis des Daemons verwendet
    - Funktioniert nur, wenn der Daemon mit einem Schwarm verbunden ist

---

# Verwendung (V)

- Und viele weitere:
    - `ipfs --help`: Hilfe
    - `ipfs commands`: Komplette Auflistung aller Kommandos

---

# Technische Hintergründe (I)

## Hash-Kollisionen
Theoretisch möglich, allerdings sehr unwahrscheinlich. Die Entwickler versuchen, die Struktur so aufzubauen, dass in Zukunft eine Migration zu anderen Hashingalgorithmen möglich ist.

## Bootstrapping
Aktuell wird eine Standard-Liste an vertrauten Bootstrapping-Nodes vorkonfiguriert, kann jedoch geändert werden.

---

# Technische Hintergründe (II)

## Referenzimplementierung in go, Spezifikation nicht sprachspezifisch

## libp2p

Die P2P-Funktionalität ist in die Bibliothek libp2p ausgelagert

## Datenrepräsentation in [Hash-Bäumen](https://de.wikipedia.org/wiki/Hash-Baum) (Merkle DAG Bäume)

- Gerichteter azyklischer Graph
    - Kanten sind eindeutige Verweise auf Objekte, und beinhalten durch den Hash alle Informationen um die Objekte verifizieren

---

# Technische Hintergründe (III)

## Transportmethoden

- Die Transportschicht ist abstrahiert
- Verschiedene Transporte bereits implementiert:
    - TCP über IPv4 und IPv6
    - UDP über IPv4 und IPv6
- In libp2p implementiert, demnächst unterstützt:
    - Websockets
- In Diskussion:
    - tor
- Künftig können selbst welche implementiert werden:
    - [Vorläufiger Link zum Dev-Branch](https://github.com/ipfs/examples/blob/bdfef47c851cdb5c41a32af80ec4d7cd0587fa49/examples/transport/readme.md)
    - [Vermutlich langfristiger Link nach Merge](https://github.com/ipfs/examples/blob/master/examples/network/readme.md)

---

# Name-Service IPNS: Theorie

- Nachteil von Hashes: ändern sich mit jeder Datei, die in einem (Unter-)Verzeichnis geändert wird.
- IPNS ist ein PKI-Namensraum
- Der Name ist der Hash des öffentlichen Sclhüssels
- Mit dem privaten Schlüssel können neue signierte Werte veröffentlicht werden
- Der Name verweist auf einen IPFS-Datei-Identifier
- Dadurch lassen sich verändernde Inhalte unter einem stabilen Identifier veröffentlichen

---

# Name-Service IPNS: Praxis

- Veröffentlichen des Hashes:
    - `ipfs name publish <ipfs-identifier>`

- Namensauflösung:
    - `ipfs name resolve <ipns-identifier>`

---

# Statisches Webhosting mit IPFS

## Namensauflösung mittels DNS:

- DNS TXT-Record hinzufügen:
    - `dnslink=/ipfs/<ipfs-identifier>` oder
    - `dnslink=/ipns/<ipns-identifier>`

- Die Seite ist nun über den Gateway aufrufbar:
    - <http://localhost:8080/ipns/your.domain>
    - <http://gateway.ipfs.io/ipns/your.domain>

- Beispiel:
    - <http://gateway.ipfs.io/ipns/ipfs.io>

---

# Kontroverses Thema

## DMCA-Takedowns, illegales oder nicht erwünschtes Material

Optionales Blacklisting mit vom Entwicklerteam gepflegten Einträgen in Verwendung bei Gateways, die vom Entwicklerteam betrieben werden. Nicht jedoch bei selbst installierten Nodes.

Hier ist mir erstmalig in freier Wildbahn der HTTP-Errorcode [451 Unavailable For Legal Reasons](https://en.wikipedia.org/wiki/HTTP_451) untergekommen.

---

# Ausblick / Ideen

- [Ethereum](https://ethereum.org/)
- [The Internet Archive](https://archive.org): [Wired-Artikel](https://www.wired.com/2016/06/inventors-internet-trying-build-truly-permanent-web/)
- [Mediachain](https://www.mediachain.io/): Blockchain, die mittels IPFS-Hashes Medienobjekte (Videos, Fotos…) referenziert
- Bezahltes IPFS-Pinning gegen "Austrocknen"
- Diskussionen über Paketupdates per IPFS
- [OpenBazaar](https://www.openbazaar.org/): Dezentrale Online-Verkaufsplattform. Wird das Backend auf IPFS migrieren
- [IPFS Pics](https://ipfs.pics/): Bildanzeigedienst für Bilder in IPFS
- Publish/Subscribe-Mechanismus ist geplant
- [IPFS-Gateway-Redirect](https://addons.mozilla.org/en-US/firefox/addon/ipfs-gateway-redirect/): Firefox-Plugin für Adressen im Format `fs:/ipfs/*`
- Meine Voraussage: Spielehersteller werden künftig Content über IPFS ausliefern
